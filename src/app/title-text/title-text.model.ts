export class TitleText {
  constructor(
    public title: string,
    public description: string
  ) {}
}
