import { Component, Input } from '@angular/core';
import { TitleText } from './title-text.model';

@Component({
  selector: 'app-title-text',
  standalone: true,
  templateUrl: './title-text.component.html',
  styleUrls: ['./title-text.component.scss'],
})
export class TitleTextComponent {
  @Input() titleText!: TitleText;
  // Soit on met le ! pour promettre qu'on initialisera titleText, soit on ajoute un constructeur :
  // constructor() {
  //   this.titleText = {
  //     title: 'Default Title',
  //     description: 'Default Description',
  //   };
  // }
}
