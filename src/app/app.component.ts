import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { TopPageComponent } from './top-page/top-page.component';
import { TitleText } from './title-text/title-text.model';
import { TitleTextComponent } from './title-text/title-text.component';
import { CardComponent } from './card/card.component';
import { Card } from './card/card.model';
import { NgForOf } from '@angular/common';
import { ImgTextContentComponent } from './img-text-content/img-text-content.component';
import { ImgTextContent } from './img-text-content/img-text-model';
import { FooterComponent } from './footer/footer.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    HeaderComponent,
    TopPageComponent,
    TitleTextComponent,
    CardComponent,
    NgForOf,
    ImgTextContentComponent,
    FooterComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  myTitleText!: TitleText;
  cardList: Card[] = [];
  imgText!: ImgTextContent;
  imgTextLeft!: ImgTextContent;
  imgTextRight!: ImgTextContent;

  ngOnInit() {
    this.myTitleText = new TitleText(
      'Pourquoi utiliser Signal ?',
      'Explorez ci-dessous afin de découvrir pourquoi Signal est une messagerie simple, puissante et sécurisée'
    );
    this.imgText = new ImgTextContent(
      '../../assets/animation.png',
      "Animation d'une conversation Signal",
      'Partager sans insécurité',
      'Un chiffrement de bout en bout de pointe (propulsé par le protocole Signal à code source ouvert) assure la sécurité de vos conversations. Nous ne pouvons ni lire vos messages ni écouter vos appels, et personne d’autre que vous ne peut le faire. La confidentialité n’est pas proposée en option, c’est simplement la façon dont Signal fonctionne. Pour tous les messages, pour tous les appels, tout le temps.'
    );
    this.cardList = [
      {
        imageSrc: '../../assets/Media.png',
        imageAlt: 'Chat qui regarde des bulles de conversation',
        title: 'Dites ce qui vous plaît',
        description:
          'Partagez gratuitement des textes, des messages vocaux, des photos, des vidéos et des GIF. Signal utilise la connexion de données de votre téléphone afin de vous éviter les frais relatifs à l’envoi de texto et de message multimédia.',
      },
      {
        imageSrc: '../../assets/Calls.png',
        imageAlt: 'Image de deux femmes se faisant un appel vidéo',
        title: 'Exprimez-vous librement',
        description:
          'Passez des appels vocaux ou vidéo clairs avec des personnes qui vivent à l’autre bout de la ville ou du monde, sans frais supplémentaires',
      },
      {
        imageSrc: '../../assets/Stickers.png',
        imageAlt: 'Dessin de chat à la plage',
        title: 'Respectez votre vie privée',
        description:
          'Avec nos stickers chiffrés, exprimez-vous encore plus librement. Vous êtes même libre de créer et de partager vos propres packs de stickers.',
      },
      {
        imageSrc: '../../assets/Groups.png',
        imageAlt: 'Bulles de conversation vides',
        title: 'Rassemblez-vous avec les groupes',
        description:
          'Avec les conversations de groupe, restez facilement en contact avec votre famille, vos amis et vos collègues.',
      },
    ];
    this.imgTextLeft = new ImgTextContent(
      '../../assets/No-Ads.png',
      'Illustration de publicités barrées',
      'Aucune publicité, aucun traqueur, vraiment.',
      'Dans Signal, il n’y a aucune publicité, aucun vendeur partenaire, ni aucun système de suivi inquiétant. Vous pouvez vous consacrer à partager les moments importants avec les personnes qui comptent pour vous.'
    );
    this.imgTextRight = new ImgTextContent(
      '../../assets/Nonprofit503.png',
      "Illustration d'un globe terrestre avec bulles de conversation",
      'Gratuit pour tous',
      'Signal est un organisme à but non lucratif indépendant. Nous ne sommes reliés à aucune entreprise technologique importante et nous ne pourrons jamais être achetés par l’une d’elles. Le développement de notre plateforme est financé par des subventions et des dons de personnes comme vous.'
    );
  }
}
