export interface Card {
  imageSrc: string;
  imageAlt: string;
  title: string;
  description: string;
}
