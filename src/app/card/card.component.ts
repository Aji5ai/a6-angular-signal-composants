import { Component, Input } from '@angular/core';
import { Card } from './card.model';
import { TitleTextComponent } from '../title-text/title-text.component';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [TitleTextComponent],
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() card!: Card;
  // Soit on ajoute le ! pour promettre que card sera initialisé plus tard, soit on met un constructeur comme suit : 
  // constructor() {
  //   this.card = {
  //     imageSrc: '',
  //     imageAlt: '',
  //     title: 'Default Title',
  //     description: 'Default Description',
  //   };
  // }
}
