export class ImgTextContent {
  constructor(
    public imageSrc: string,
    public imageAlt: string,
    public title: string,
    public description: string
  ) {}
}
