import { Component, Input } from '@angular/core';
import { TitleTextComponent } from '../title-text/title-text.component';
import { ImgTextContent } from './img-text-model';

@Component({
  selector: 'app-img-text-content',
  standalone: true,
  imports: [TitleTextComponent],
  templateUrl: './img-text-content.component.html',
  styleUrl: './img-text-content.component.scss',
})
export class ImgTextContentComponent {
  @Input() imgTextContent!: ImgTextContent;
}
