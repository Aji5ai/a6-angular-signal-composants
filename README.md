# A5AngularBankBasics

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 17.2.1.

## Installation d'Angular

Si Angular n'est pas encore installé sur votre ordinateur, veuillez exécuter la commande suivante dans un terminal pour l'installer en global :
`npm install -g @angular/cli`

Pour vérifier votre version d'Angular : `ng v`

## Création d'un projet Angular
Une fois installé, ouvrez votre terminal dans le dossier souhaité. La création d'un projet Angular créera également un nouveau dossier pour le projet.

Pour créer un projet Angular sans tests et en SCSS :
`ng new application-name --style=scss --skip-tests=true`

## Installer le projet importé
Après avoir cloné le projet, sur votre terminal, exécutez la commande : `npm ci` pour installer les dépendances du projet.

## Serveur de développement

Exécutez `ng serve` pour un serveur dev. Le site devrait être accessible à l'adresse `http://localhost:4200/`. L'application sera automatiquement rechargée si vous modifiez l'un des fichiers sources.

## Code scaffolding
Exécutez `ng generate component component-name` ou `ng g c <component-name>` pour générer un nouveau composant. Vous pouvez également utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Créer une classe 
`ng g class <class-name>`

## Build

Lancez `ng build` pour construire le projet. Les build artifacts seront stockés dans le répertoire `dist/`.

## Exécution des tests unitaires

Lancer `ng test` pour exécuter les tests unitaires via [Karma](https://karma-runner.github.io).

## Exécution des tests de bout en bout

Lancez `ng e2e` pour exécuter les tests end-to-end via la plateforme de votre choix. Pour utiliser cette commande, vous devez d'abord ajouter un paquet qui implémente les capacités de test end-to-end.

## Aide supplémentaire

Pour obtenir plus d'aide sur la CLI Angular, utilisez `ng help` ou allez voir la page [Angular CLI Overview and Command Reference](https://angular.io/cli).